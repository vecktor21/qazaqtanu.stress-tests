from locust import HttpUser, task, between

class ShopUser(HttpUser):
    wait_time = between(1, 5)

    @task(1)
    def get_client(self):
        client_id = "40dc1d70-9232-4713-abf0-8104e4b60aed"  # Replace with appropriate client id
        self.client.get(f"/api/shop/client/{client_id}")

    @task(2)
    def post_balance(self):
        client_id = "40dc1d70-9232-4713-abf0-8104e4b60aed"  # Replace with appropriate client id
        self.client.post(f"/api/shop/client/{client_id}/balance", json={"amount": 10000})

    @task(3)
    def get_item_by_id(self):
        item_id = "some_item_id"  # Replace with appropriate item id
        self.client.get(f"/api/shop/items/{item_id}")

    @task(4)
    def get_items(self):
        self.client.get("/api/shop/items")

    @task(7)
    def get_items_by_category(self):
        category = "computers"  # Replace with appropriate category
        self.client.get(f"/api/shop/items/category/{category}")

    @task(8)
    def post_cart(self):
        client_id = "40dc1d70-9232-4713-abf0-8104e4b60aed"  # Replace with appropriate client id
        item_id = "31bc476b-0969-441c-87b0-a07f77528d29"  # Replace with appropriate item id
        self.client.post(f"/api/shop/client/{client_id}/cart/{item_id}")

    @task(9)
    def delete_cart_item(self):
        client_id = "40dc1d70-9232-4713-abf0-8104e4b60aed"  # Replace with appropriate client id
        item_id = "31bc476b-0969-441c-87b0-a07f77528d29"  # Replace with appropriate item id
        self.client.delete(f"/api/shop/client/{client_id}/cart/{item_id}")

    @task(10)
    def get_orders(self):
        client_id = "40dc1d70-9232-4713-abf0-8104e4b60aed"  # Replace with appropriate client id
        self.client.get(f"/api/shop/client/{client_id}/orders")

    @task(11)
    def post_orders(self):
        client_id = "40dc1d70-9232-4713-abf0-8104e4b60aed"  # Replace with appropriate client id
        self.client.post(f"/api/shop/client/{client_id}/orders", json={"order_id": "some_order_id"})
